#!/usr/bin/env python

from argparse import ArgumentParser, FileType
from traceback import print_exc
import pandas as pd
import matplotlib.pyplot as plt
import sys

def main(args):
  summary = pd.read_csv(args.summary)
  data = pd.read_csv(args.input)
  objs = data.groupby(['id'])
  for (kid, obj) in objs:
    obj = remove_outliers(obj, args.clip)
    write_phase_plot(obj, kid, args.plot, summary)

def remove_outliers(obj, n):
  obj = obj[abs(obj.flux - obj.flux.mean()) < n * obj.flux.std()]
  return obj

def write_phase_plot(obj, kid, prefix, summary):
  fname = '.'.join((prefix, str(kid), 'png'))
  print >> sys.stderr, ("writing %s" % fname)
  row = summary[summary.id == kid]
  title = "%s (T = %.2f, FDP = %g)" % (kid, row.period, row.p_value)
  fig = plt.figure()
  fig.suptitle(title)
  plt.xlabel('phase (days)')
  plt.ylabel('flux')
  plt.plot(obj.phase, obj.flux, 'k.')
  plt.savefig(fname, bbox_inches='tight')
  plt.close(fig)

if __name__ == '__main__':
  p = ArgumentParser()
  p.add_argument('-c', '--clip',
    metavar="INTEGER",
    type=float,
    default=3.0,
    help="outlier threshold in units of standard deviation")
  p.add_argument('-s', '--summary',
    metavar="INPUT_FILE",
    type=FileType('r'),
    required=True,
    help="the file containing period, power and p-value data")
  p.add_argument("-p", "--plot",
    type=str,
    default="phase_plot",
    metavar="OUTPUT_FILE",
    help="prefix for plot file names")
  p.add_argument("input",
    metavar="INPUT_FILE",
    default=sys.stdin,
    type=FileType('r'),
    help="input file containing phase-flux data")
  args = p.parse_args()
  try:
    main(args)
    res = 0;
  except:
    print_exc()
    res = -1
  finally:
    sys.exit(res)
