#!/usr/bin/env python

from astropy.io import fits
from argparse import ArgumentParser, FileType
from PyAstronomy.pyTiming.pyPeriod import Gls, TimeSeries
from traceback import print_exc
from math import exp
import numpy as np
import sys
import csv


def fit_light_curve(hdul, args):
  t = hdul[1].data['TIME']
  f = hdul[1].data['PDCSAP_FLUX']
  e = hdul[1].data['PDCSAP_FLUX_ERR']
  t = t - np.nanmin(t)
  mask = np.logical_not(np.logical_or(np.isnan(t), np.isnan(f)))
  lc = TimeSeries(t[mask], f[mask], error=e[mask])
  model = Gls(lc, ofac=args.oversampling, hifac=args.nyquist)
  idx = np.argmax(model.power)
  hdul[0].header['PERIOD'] = 1.0/model.freq[idx]
  hdul[0].header['P_VALUE'] = model.FAP(model.power[idx])
  hdul[0].header['POWER'] = model.power[idx]
  return hdul

def write_files(lcs, args):
  summary = csv.writer(args.summary, delimiter=',')
  preds = csv.writer(args.predictions, delimiter=',')
  summary.writerow(('id', 'ra', 'dec', 'period', 'power', 'p_value'))
  preds.writerow(('id', 'phase', 'flux', 'err'))

  for lc in lcs:

    # output summary line for this object
    kid = lc[0].header['KEPLERID']
    ra = lc[0].header['RA_OBJ']
    dec = lc[0].header['DEC_OBJ']
    T = lc[0].header['PERIOD']
    power = lc[0].header['POWER']
    alpha = lc[0].header['P_VALUE']
    summary.writerow((kid, ra, dec, T, power, alpha))
    
    # write out measured and predicted 
    # flux values for this object    
    phi = lc[1].data['TIME'] % T
    flux = lc[1].data['PDCSAP_FLUX']
    err = lc[1].data['PDCSAP_FLUX_ERR']
    for i in range(len(lc[1].data['TIME'])):
      preds.writerow((kid, phi[i], flux[i], err[i]))



def main(args):
  lcs = (fit_light_curve(hdul, args) for hdul in args.files)
  write_files(lcs, args)
  


if __name__ == '__main__':
  p = ArgumentParser()
  p.add_argument("-o", "--oversampling",
    metavar="INTEGER",
    type=int,
    default=10,
    help="oversampling level")
  p.add_argument("-n", "--nyquist",
    metavar="INTEGER",
    type=int,
    default=1,
    help="nyquist factor")
  p.add_argument("-s", "--summary",
    type=FileType("w"),
    required=True,
    metavar="OUTPUT_FILE",
    help="output file for computed periods")
  p.add_argument("-p", "--predictions",
    type=FileType("w"),
    required=True,
    metavar="OUTPUT_FILE",
    help="output file for computed periods")
  p.add_argument("files",
    nargs="+",
    metavar="INPUT_FILE",
    type=fits.open)
  args = p.parse_args()
  try:
    main(args)
    res = 0;
  except:
    print_exc()
    res = -1
  finally:
    sys.exit(res)
